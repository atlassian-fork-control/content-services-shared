# Fabric Content Services shared tools and utilities

## ASAP Token generator

A script to generate Bearer token for your service in given environment.

### Usage in CLI

Clone this repo and run `npm link` to make the function available in your terminal system wide:

    git clone git@bitbucket.org:atlassian/content-services-shared.git
    cd content-services-shared
    npm link

    # script is now available everywhere:
    asap-generate-token -h

### Usage in JavaScript project

Add this library to your project:

    npm install @atlassian/content-services-shared --save-dev

Add command to your npm scripts in `package.json` as follows:

    "scripts": {
      "asap:generate:token": "./node_modules/.bin/asap-generate-token -e stg-east -s pf-content-service -t 3600"
    }

## General usage

    ./asap-generate-token.sh -s <service> -e <environment> -t <expire_time>

E.g.:

    ./asap-generate-token.sh -s pf-content-service -e stg-east -t 3600

Run `./asap-generate-token.sh -h` for more info.

## Contribute

### Publishing package

- Document your changes in README.md
- Increase version in `package.json`
- Run `npm publish` command
